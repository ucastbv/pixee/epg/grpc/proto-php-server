<?php
# Generated by the protocol buffer compiler (roadrunner-server/grpc). DO NOT EDIT!
# source: epg.proto

namespace Tellie;

use Spiral\RoadRunner\GRPC;

interface EpgInterface extends GRPC\ServiceInterface
{
    // GRPC specific service name.
    public const NAME = "tellie.Epg";

    /**
    * @param GRPC\ContextInterface $ctx
    * @param \Google\Protobuf\GPBEmpty $in
    * @return \Tellie\Epg\Api\ListDatasourcesResponse
    *
    * @throws GRPC\Exception\InvokeException
    */
    public function listDatasources(GRPC\ContextInterface $ctx, \Google\Protobuf\GPBEmpty $in): \Tellie\Epg\Api\ListDatasourcesResponse;

    /**
    * @param GRPC\ContextInterface $ctx
    * @param \Tellie\Epg\Api\ListChannelsByDatasourceRequest $in
    * @return \Tellie\Epg\Api\ListChannelsByDatasourceResponse
    *
    * @throws GRPC\Exception\InvokeException
    */
    public function listChannelsByDatasource(GRPC\ContextInterface $ctx, \Tellie\Epg\Api\ListChannelsByDatasourceRequest $in): \Tellie\Epg\Api\ListChannelsByDatasourceResponse;

    /**
    * @param GRPC\ContextInterface $ctx
    * @param \Tellie\Epg\Api\ListBroadcastsRequest $in
    * @return \Tellie\Epg\Api\ListBroadcastsResponse
    *
    * @throws GRPC\Exception\InvokeException
    */
    public function listBroadcasts(GRPC\ContextInterface $ctx, \Tellie\Epg\Api\ListBroadcastsRequest $in): \Tellie\Epg\Api\ListBroadcastsResponse;
}
